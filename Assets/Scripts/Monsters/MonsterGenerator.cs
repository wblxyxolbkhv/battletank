using System;
using System.Collections.Generic;
using System.Linq;
using Damage;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Monsters
{
    /// <summary>
    /// Struct for using in Editor
    /// </summary>
    [Serializable]
    public class MonsterGenerationPreference
    {
        public GameObject monsterPrefab;
        public int generationPriority;
    }
    
    public class MonsterGenerator : MonoBehaviour
    {
        public int maxMonsterCount = 10;
        
        // every preference is a pair - prefab and priority, 
        // priority increase chance to generate exact monster
        public List<MonsterGenerationPreference> monstersPrefabs;
        
        public GameObject spawnEffectPrefab;
        
        public event Action AnyMonsterDead;
        
        
        private int _currentMonsterCount = 0;
        private BoxCollider[] _boxColliders;

        private void Start()
        {
            _boxColliders = GetComponents<BoxCollider>();
        }

        private void Update()
        {
            if (_currentMonsterCount < maxMonsterCount)
                GenerateMonster();
        }

        private void GenerateMonster()
        {
            var position = GetGenerationPoint();

            var prioritiesSum = monstersPrefabs.Aggregate(
                0,
                (sum, p) => sum + p.generationPriority
            );

            var randomPriority = Random.Range(0, prioritiesSum);
            var currentPriority = 0;
            GameObject selectedMonster = null;
            foreach (var monsterPreference in monstersPrefabs)
            {
                if (currentPriority + monsterPreference.generationPriority > randomPriority)
                {
                    selectedMonster = monsterPreference.monsterPrefab;
                    break;
                }
                else
                {
                    currentPriority += monsterPreference.generationPriority;
                }
            }
            
            var monsterObject = Instantiate(
                selectedMonster,
                position,
                Quaternion.identity
            );
            if (spawnEffectPrefab != null)
                Instantiate(
                    spawnEffectPrefab,
                    position,
                    Quaternion.identity,
                    monsterObject.transform
                );
            var monsterHealth = monsterObject.GetComponent<Health>();
            monsterHealth.Death += OnAnyMonsterDeath;
            
            
            _currentMonsterCount += 1;
        }

        private void OnAnyMonsterDeath()
        {
            _currentMonsterCount -= 1;
            AnyMonsterDead?.Invoke();
        }

        private Vector3 GetGenerationPoint()
        {
            // randomly select one of colliders
            var index = Random.Range(0, _boxColliders.Length);
            var boxCollider = _boxColliders[index];

            var globalCenter = transform.position + boxCollider.center;
            
            // calculate two opposite corners of collider
            var corner1 = globalCenter + boxCollider.size / 2;
            var corner2 = globalCenter - boxCollider.size / 2;

            // select random point between opposite corners
            var randomX = Random.Range(
                Mathf.Min(corner1.x, corner2.x),
                Mathf.Max(corner1.x, corner2.x)
            );
            
            var randomY = Random.Range(
                Mathf.Min(corner1.y, corner2.y),
                Mathf.Max(corner1.y, corner2.y)
            );
            
            var randomZ = Random.Range(
                Mathf.Min(corner1.z, corner2.z),
                Mathf.Max(corner1.z, corner2.z)
            );
            
            return new Vector3(randomX, randomY, randomZ);
        }
    }
}
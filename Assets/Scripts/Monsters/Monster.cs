﻿using Damage;
using UnityEngine;
using UnityEngine.AI;

namespace Monsters
{
    public class Monster : MonoBehaviour
    {
        /// <summary>
        /// Disable all generated monsters on map
        /// </summary>
        public static void DisableAllMonsters()
        {
            var monsters = Object.FindObjectsOfType<Monster>();
            foreach (var monster in monsters)
            {
                monster.DisableMonster();
            }
        }


        /// <summary>
        /// Disable monster - stop chasing player and animation
        /// </summary>
        public void DisableMonster()
        {
            var animator = GetComponentInChildren<Animator>();
            animator.enabled = false;

            _navMeshAgent.isStopped = true;
        }
        
        
        private NavMeshAgent _navMeshAgent;

        private Tank.Tank _target;
        
        private void Start()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            var health = GetComponent<Health>();
            health.Death += OnDeath;
        }

        private void OnDeath()
        {
            var touchDamager = GetComponent<TouchDamager>();
            touchDamager.DestroyDamager();
        }

        private void Update()
        {
            if (_target == null)
                _target = FindTarget();
            
            if (_target == null)
                return;
            
            // user nav mesh agent for find path to player
            _navMeshAgent.SetDestination(_target.transform.position);
        }

        private Tank.Tank FindTarget()
        {
            var tank = FindObjectOfType<Tank.Tank>();
            if (tank != null)
                return tank;
            return null;
        }
    }
}

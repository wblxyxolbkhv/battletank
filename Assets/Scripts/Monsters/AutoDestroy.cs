using System;
using System.Collections;
using UnityEngine;

namespace Monsters
{
    /// <summary>
    /// Util component, destroy object after delay
    /// </summary>
    public class AutoDestroy : MonoBehaviour
    {
        public float destroyDelay;

        private void Start()
        {
            StartCoroutine(DestroyCoroutine());
        }

        private IEnumerator DestroyCoroutine()
        {
            yield return new WaitForSeconds(destroyDelay);
            Destroy(gameObject);
        }
    }
}
using System;
using UnityEngine;
using System.Collections;

namespace CameraUtils
{
    /// <summary>
    /// Isometric camera which follow target 
    /// </summary>
    public class FollowingCamera : MonoBehaviour {

        public Transform target;
        public float distance = 10.0f;
        public float height = 5.0f;
        
        public float cameraSpeed = 2.0f;

        private Vector3 _cameraDifference;
        
        private void Start()
        {
            _cameraDifference = new Vector3(0, height, -distance);
        }

        private void FixedUpdate () {
            if (!target) return;

            transform.position = Vector3.Lerp(
                transform.position, 
                target.position + _cameraDifference,
                    cameraSpeed * Time.deltaTime
                );
            
            transform.LookAt(target);
        }
    }
}
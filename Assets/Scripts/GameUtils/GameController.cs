﻿using System.Collections;
using System.Collections.Generic;
using Damage;
using Monsters;
using Tank;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameUtils
{
    /// <summary>
    /// Main game controller, singleton
    /// </summary>
    public class GameController : MonoBehaviour
    {
        public Health player;

        public Canvas mainCanvas;
        public GameObject gameOverPanelPrefab;

        public Text scoreText;

        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void Exit()
        {
            Application.Quit();
        }


        public static GameController Shared;


        private int _score = 0;
        
        private void Start()
        {
            Shared = this;
            player.Death += OnPlayerDeath;
            var monsterGenerator = FindObjectOfType<MonsterGenerator>();
            monsterGenerator.AnyMonsterDead += OnAnyMonsterDead;
        }

        private void OnAnyMonsterDead()
        {
            _score += 1;
            scoreText.text = $"Score: {_score}";
        }

        private void OnPlayerDeath()
        {
            Monster.DisableAllMonsters();

            var playerController = FindObjectOfType<BaseTankController>();
            playerController.enabled = false;
            
            Instantiate(
                gameOverPanelPrefab,
                mainCanvas.transform.position,
                Quaternion.identity,
                mainCanvas.transform
            );
        }
    }
}

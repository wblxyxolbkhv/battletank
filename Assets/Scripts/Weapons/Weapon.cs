using System.Collections;
using System.Collections.Generic;
using Damage;
using UnityEngine;

namespace Weapons
{
    public class Weapon : MonoBehaviour
    {
        // all weapon settings in one place
        
        public GameObject bulletPrefab;
        
        // places when bullet will spawned, if more then one, points have select rotation
        public List<GameObject> bulletSpawnPoints;
        
        public GameObject bulletExplosionPrefab;
        public float damage;
        public float timeBetweenShots;
        public float bulletSpeed;

        public void Attack(Vector3 direction)
        {
            if (!_ready)
                return;

            var bullet = Instantiate(
                bulletPrefab,
                GetNextSpawnPoint(),
                Quaternion.LookRotation(direction)
            );
            var bulletDamager = bullet.GetComponent<TouchDamager>();
            bulletDamager.damage = damage;
            bulletDamager.destroyEffectPrefab = bulletExplosionPrefab;
            var bulletBody = bullet.GetComponent<Rigidbody>();
            bulletBody.velocity = direction * bulletSpeed;
            
            // to limit shooting speed, block attack for period
            _ready = false;
            StartCoroutine(WaitWeaponReady());
        }

        private bool _ready = true;
        private int _bulletSpawnPointIndex = 0;

        private IEnumerator WaitWeaponReady()
        {
            yield return new WaitForSeconds(timeBetweenShots);
            _ready = true;
        }

        private Vector3 GetNextSpawnPoint()
        {
            var point = bulletSpawnPoints[_bulletSpawnPointIndex];
            _bulletSpawnPointIndex += 1;
            if (_bulletSpawnPointIndex == bulletSpawnPoints.Count)
                _bulletSpawnPointIndex = 0;
            return point.transform.position;
        }
    }
}
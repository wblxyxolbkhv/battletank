using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weapons
{
    /// <summary>
    /// Controller for changing several weapons
    /// </summary>
    public class WeaponsController : MonoBehaviour
    {
        public List<GameObject> weapons;
        public GameObject weaponSpawnPoint;
        public float changeWeaponDelay = 0.2f;

        public Weapon CurrentWeapon => _currentWeapon;

        public void NextWeapon()
        {
            ChangeWeapon(_currentWeaponIndex + 1);
        }
        
        public void PreviousWeapon()
        {
            ChangeWeapon(_currentWeaponIndex - 1);
        }
        
        
        
        
        private Weapon _currentWeapon;
        private int _currentWeaponIndex;
        private bool _changeWeaponBlocked = false;

        private void Start()
        {
            ChangeWeapon(0);
        }

        private void ChangeWeapon(int index)
        {
            if (_changeWeaponBlocked)
                return;
            
            if (index >= weapons.Count)
                index = 0;
            if (index < 0)
                index = weapons.Count - 1;
            
            if (_currentWeapon != null)
                Destroy(_currentWeapon.gameObject);

            var newWeaponObj = Instantiate(
                original: weapons[index],
                position: weaponSpawnPoint.transform.position,
                parent: transform,
                rotation: transform.rotation
            );
            _currentWeapon = newWeaponObj.GetComponent<Weapon>();
            _currentWeaponIndex = index;
            
            _changeWeaponBlocked = true;
            StartCoroutine(WaitUnblockChangeWeapon());
        }

        private IEnumerator WaitUnblockChangeWeapon()
        {
            yield return new WaitForSeconds(changeWeaponDelay);
            _changeWeaponBlocked = false;
        }
    }
}
using System;
using GameUtils;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GameOverPanel : MonoBehaviour
    {
        public Button restartButton;
        public Button exitButton;

        private void Start()
        {
            restartButton.onClick.AddListener(() =>
            {
                GameController.Shared.Restart();
            });
            
            exitButton.onClick.AddListener(() =>
            {
                GameController.Shared.Exit();
            });
        }
    }
}
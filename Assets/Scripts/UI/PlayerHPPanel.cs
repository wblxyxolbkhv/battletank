using System;
using Damage;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    /// <summary>
    /// UI panel show player HP, decrease size with HP percent
    /// </summary>
    public class PlayerHPPanel : MonoBehaviour
    {
        public Text hpText;
        public Image hpPanel;

        public Health target;


        private RectTransform _rectTransform;
        private float _maxWidth;
        
        private void Start()
        {
            _rectTransform = hpPanel.GetComponent<RectTransform>();
            _maxWidth = _rectTransform.rect.width;
        }

        private void Update()
        {
            hpText.text = $"{Mathf.RoundToInt(target.CurrentHealth)}/{Mathf.RoundToInt(target.baseHealth)}";

            var percent = target.CurrentHealth / target.baseHealth;
            
            _rectTransform.offsetMax = new Vector2(
                -(1 - percent) * _maxWidth, 
                _rectTransform.offsetMax.y);
        }
    }
}
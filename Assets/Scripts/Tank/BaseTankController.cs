using System;
using UnityEngine;
using Weapons;

namespace Tank
{
    /// <summary>
    /// Base controller for control tank, inherit to
    /// implement AI, Joystick or multi player tank control
    /// </summary>
    public abstract class BaseTankController : MonoBehaviour
    {
        public abstract Vector3 GetDirection();
        public abstract bool GetAttack();
        public abstract bool GetNextWeapon();
        public abstract bool GetPreviousWeapon();
        
        
        protected Tank Tank;
        protected WeaponsController TankWeaponsController;
        
        protected virtual void Awake()
        {
            Tank = GetComponent<Tank>();
            TankWeaponsController = GetComponent<WeaponsController>();
        }

        protected virtual void FixedUpdate()
        {
            var direction = GetDirection();
            if (direction != Vector3.zero)
            {
                Tank.MoveTank(direction.normalized);
            }
            else
            {
                Tank.StopTank();
            }
            
            if (GetAttack())
                Tank.Attack();
            
            if (GetNextWeapon())
                TankWeaponsController.NextWeapon();
            
            if (GetPreviousWeapon())
                TankWeaponsController.PreviousWeapon();
        }
    }
}
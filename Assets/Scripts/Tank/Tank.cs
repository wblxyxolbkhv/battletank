using System;
using Damage;
using UnityEngine;
using Weapons;

namespace Tank
{
    public class Tank : MonoBehaviour
    {
        public float moveSpeed = 10;
        public float accelerationSpeed = 2;
        public float stopSpeed = 2;
        public float rotationSpeed = 2;
        
        
        public void MoveTank(Vector3 direction)
        {
            RotateTank(direction);

            _rigidbody.velocity = Vector3.Lerp(
                _rigidbody.velocity,
                direction * moveSpeed,
                accelerationSpeed * Time.fixedDeltaTime
            );
        }

        public void RotateTank(Vector3 direction)
        {
            transform.rotation = Quaternion.Lerp(
                transform.rotation,
                Quaternion.LookRotation(direction),
                rotationSpeed * Time.fixedDeltaTime
                );
        }
        
        public void StopTank()
        {
            _rigidbody.velocity = Vector3.Lerp(
                _rigidbody.velocity,
                Vector3.zero,
                stopSpeed * Time.fixedDeltaTime
            );
        }
        
        public void Attack()
        {
            if (_weaponsController.CurrentWeapon == null)
                return;
            
            _weaponsController.CurrentWeapon.Attack(transform.forward);
        }
        

        

        private Rigidbody _rigidbody;
        private WeaponsController _weaponsController;
        
        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _weaponsController = GetComponent<WeaponsController>();

            var health = GetComponent<Health>();
            health.Death += OnDeath;
        }

        private void OnDeath()
        {
            Debug.Log("Game Over!");
        }
    }
}
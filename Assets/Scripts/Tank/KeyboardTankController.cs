﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tank
{
    /// <summary>
    /// Control for PC keyboard, keys can be changes in editor
    /// </summary>
    public class KeyboardTankController : BaseTankController
    {
        public KeyCode moveUpKey = KeyCode.W;
        public KeyCode moveLeftKey = KeyCode.A;
        public KeyCode moveDownKey = KeyCode.S;
        public KeyCode moveRightKey = KeyCode.D;

        public KeyCode previousWeaponKey = KeyCode.Q;
        public KeyCode nextWeaponKey = KeyCode.E;

        public KeyCode attackKey = KeyCode.X;


        public override Vector3 GetDirection()
        {
            _activeKeys.Clear();
            foreach (var key in _moveKeys)
            {
                if (Input.GetKey(key))
                    _activeKeys.Add(key);
            }

            if (_activeKeys.Any())
            {
                // calculate result direction vector, not only up, down, left and right 
                // directions are permitted, also up-left, up-right..
                var direction = _activeKeys.Select(k => _moveKeysDirections[k])
                    .Aggregate((v1, v2) => v1 + v2);

                return direction;
            }
            else
            {
                return Vector3.zero;
            }
        }

        public override bool GetAttack()
        {
            return Input.GetKey(attackKey);
        }

        public override bool GetNextWeapon()
        {
            return Input.GetKey(nextWeaponKey);
        }

        public override bool GetPreviousWeapon()
        {
            return Input.GetKey(previousWeaponKey);
        }


        private KeyCode[] _moveKeys;
        private Dictionary<KeyCode, Vector3> _moveKeysDirections = new Dictionary<KeyCode, Vector3>();
        private List<KeyCode> _activeKeys = new List<KeyCode>();

        protected override void Awake()
        {
            base.Awake();
            _moveKeys = new[] { moveUpKey, moveDownKey, moveLeftKey, moveRightKey };
            
            _moveKeysDirections[moveUpKey] = Vector3.forward;
            _moveKeysDirections[moveDownKey] = Vector3.back;
            _moveKeysDirections[moveLeftKey] = Vector3.left;
            _moveKeysDirections[moveRightKey] = Vector3.right;
        }
    }
}

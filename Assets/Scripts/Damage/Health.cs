using System;
using UnityEngine;

namespace Damage
{
    /// <summary>
    /// Component for getting damage
    /// </summary>
    public class Health : MonoBehaviour
    {
        public float baseHealth = 100;
        public float armor = 0.5f;

        public event Action Death;

        public float CurrentHealth => _currentHealth;

        public bool IsDead => _currentHealth < 0.001;

        public void Damage(float damage)
        {
            if (IsDead)
                return;
            
            var realArmor = Mathf.Min(Mathf.Max(0, armor), 1);
            _currentHealth -= damage * (1 - realArmor);
            _currentHealth = Mathf.Max(_currentHealth, 0);
            if (IsDead)
            {
                Death?.Invoke();
            }
        }

        
        private float _currentHealth;
        private void Start()
        {
            _currentHealth = baseHealth;
        }
    }
}
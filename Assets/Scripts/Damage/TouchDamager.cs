using System;
using System.Collections;
using UnityEngine;

namespace Damage
{
    /// <summary>
    /// Component, which deals damage when touch enemy
    /// </summary>
    public class TouchDamager : MonoBehaviour
    {
        public float damage;
        public LayerMask layers;
        public bool autoDestroy = true;
        public float damagePeriod = 1;
        
        public GameObject destroyEffectPrefab;

        /// <summary>
        /// Destroy object with destroy effect
        /// </summary>
        public void DestroyDamager()
        {
            Destroy(gameObject);
            
            if (destroyEffectPrefab)
            {
                Instantiate(
                    destroyEffectPrefab,
                    transform.position,
                    Quaternion.identity
                );
            }
        }


        private bool _damageBlocked = false;
        
        private void OnCollisionEnter(Collision other)
        {
            HandleCollision(other);
        }

        private void OnCollisionStay(Collision other)
        {
            HandleCollision(other);
        }

        private void HandleCollision(Collision other)
        {
            if (((1 <<other.gameObject.layer) & layers.value) == 0)
                return;
            
            if (_damageBlocked)
                return;
            
            var health = other.gameObject.GetComponent<Health>();
            if (health != null)
                health.Damage(damage);
            
            if (autoDestroy)
                DestroyDamager();
            else
            {
                // to prevent damage in every frame, block damage for period
                _damageBlocked = true;
                StartCoroutine(WaitUnblockDamage());
            }
        }

        private IEnumerator WaitUnblockDamage()
        {
            yield return new WaitForSeconds(damagePeriod);
            _damageBlocked = false;
        }
    }
}
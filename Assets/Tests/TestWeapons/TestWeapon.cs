using System;
using System.Collections;
using Damage;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Weapons;
using Object = UnityEngine.Object;

namespace Tests.TestWeapons
{
    public class TestWeapon
    {
        private Weapon _weapon;

        [SetUp]
        public void Setup()
        {
            var prefab = Resources.Load<GameObject>("Prefabs/Weapons/TurretWeapon");
            GameObject weaponObject = Object.Instantiate(
                prefab
            );
            _weapon = weaponObject.GetComponent<Weapon>();
        }

        [UnityTest]
        public IEnumerator TestTimeBetweenShots()
        {
            Assert.NotNull(_weapon);

            _weapon.timeBetweenShots = 1f;
            
            var bullets = Object.FindObjectsOfType<TouchDamager>();
            Assert.AreEqual(0, bullets.Length);
            
            _weapon.Attack(Vector3.back);
            bullets = Object.FindObjectsOfType<TouchDamager>();
            Assert.AreEqual(1, bullets.Length);
            
            yield return Wait(0.5f);
            _weapon.Attack(Vector3.back);
            bullets = Object.FindObjectsOfType<TouchDamager>();
            Assert.AreEqual(1, bullets.Length);
            
            _weapon.Attack(Vector3.forward);
            yield return Wait(0.6f);
            bullets = Object.FindObjectsOfType<TouchDamager>();
            Assert.AreEqual(2, bullets.Length);
        }

        public IEnumerator Wait(float seconds)
        {
            DateTime startTime = DateTime.UtcNow;
            do
            {
                yield return null;
            }
            while ((DateTime.UtcNow - startTime).TotalSeconds < seconds);
        }
    }

}
﻿﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using Tank;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestKeyboardTankController: InputTestFixture
    {
        private Keyboard _keyboard;
        private KeyboardTankController _keyboardController;
        private Tank.Tank _tank;

        public override void Setup()
        {
            base.Setup();

            var prefab = Resources.Load<GameObject>("Prefabs/Player/PlayerTank");
            GameObject playerTank = Object.Instantiate(
                prefab
            );
            _keyboardController = playerTank.GetComponent<KeyboardTankController>();
            _tank = playerTank.GetComponent<Tank.Tank>();
            _keyboard = InputSystem.AddDevice<Keyboard>();
        }

        [UnityTest]
        public IEnumerator TestUp()
        {
            Press(_keyboard.wKey);
            Assert.AreEqual(_keyboardController.GetDirection(), Vector3.forward);
            yield return null;
        }
    }
}
